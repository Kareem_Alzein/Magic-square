// numbers array
let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9],
    n = 3;
//n*n square with 0 in it's cells
const initialSquare = new Array(n).fill(0).map(() => new Array(n).fill(0));
let MovesArray = []
let stepsArray = []
let MagicNumber = []

// get sum of any one dimensonial array
function getArraySum(array) {
    let sum = array.reduce((partial_sum, a) => {
        return partial_sum + a
    }, 0)
    return sum
}

//puts number in a cell in certain square
function moveTo(numbers, square, i, j) {
    let lastSquare = JSON.parse(JSON.stringify(square))
    lastSquare[i][j] = numbers[0];
    return lastSquare
}

//updates magic number
function updateMagicNumber(number, MagicNumber) {
    MagicNumber.push(number)
    return MagicNumber
}

//push matrix into array of matrices
function updateArray(array, bigArray) {
    bigArray.push(array)
    return bigArray
}

//initializing the empty square adding the initial square to movesArray that contains the path for right answer
updateArray(initialSquare, MovesArray)
updateMagicNumber(0, MagicNumber)



function canPut(numbers, i, j, MagicNumber, array) {
    //take a copy of array given
    let tmpSquare = JSON.parse(JSON.stringify(array))
    // check if cell is empty and there are numbers left
    if (tmpSquare[i][j] == 0 && numbers.length != 0) {
        //get sum of row and column and diagonals of this cell
        let row = getArraySum(tmpSquare[i])
        let col = getArraySum(tmpSquare.map(val => val[j]))
        let diagonals = calDiagonals(tmpSquare)

        let rowStatus = true, colStatus = true;
        //check if row is full
        tmpSquare[i].map(val => {
            if (val == 0) rowStatus = false
        })
        //check if column is full
        tmpSquare.map(val => {
            val.map((el, index) => {
                if (index == j) {
                    if (el == 0) {
                        colStatus = false
                    }
                }
            })
        })
        //check if diagonals are full
        let DiaStatus = false
        let DiaStatus2 = false
        if (i = j) {
            DiaStatus = true
            for (let row = 0; row < tmpSquare.length; row++) {
                if (tmpSquare[row][row] == 0) {
                    DiaStatus = false
                }
            }
        }
        if (tmpSquare.length == i + j + 1) {
            DiaStatus2 = true
            for (let row = 0; row < tmpSquare.length; row++) {
                if (tmpSquare[row][tmpSquare.length - row - 1] == 0) {
                    DiaStatus2 = false
                }
            }
        }
        //if row is full
        if (rowStatus) {
            //if magic number is 0 then it's the first row completed
            if (MagicNumber[MagicNumber.length - 1] == 0) {
                //make magic number same as row sum
                updateMagicNumber(row, MagicNumber);
                return true
                // if magic number larger than 0 and magic number != row
            } else if (MagicNumber[MagicNumber.length - 1] > 0 && MagicNumber[MagicNumber.length - 1] != row) {
                return console.log("Can't Place Number Here");
            }
        }

        // same logic for column and diagonals
        if (colStatus) {
            if (MagicNumber[MagicNumber.length - 1] == 0) {
                updateMagicNumber(col, MagicNumber);
                return true
            } else if (MagicNumber[MagicNumber.length - 1] > 0 && MagicNumber[MagicNumber.length - 1] != col) {
                return console.log("Can't Place Number Here");
            }
        }
        if (DiaStatus) {
            if (MagicNumber[MagicNumber.length - 1] == 0) {
                updateMagicNumber(diagonals[0], MagicNumber);
                return true
            } else if (MagicNumber[MagicNumber.length - 1] > 0 && MagicNumber[MagicNumber.length - 1] != diagonals[0]) {
                return console.log("Can't Place Number Here");
            }
        }
        if (DiaStatus2) {
            if (MagicNumber[MagicNumber.length - 1] == 0) {
                updateMagicNumber(diagonals[1], MagicNumber);
                return true
            } else if (MagicNumber[MagicNumber.length - 1] > 0 && MagicNumber[MagicNumber.length - 1] != diagonals[1]) {
                return console.log("Can't Place Number Here");
            }
        }
        //if it didn't enter all the if statements then there is no problem adding the number
        return true
    } else {
        return "This Place Is Not Empty"
    }
}

//function calculates every row sum
function calHor(array) {
    var rowSum = array.map(r => r.reduce((a, b) => a + b));
    return rowSum
}
//function calculates every columns sum
function calVer(array) {
    var colSum = array.reduce((a, b) => a.map((x, i) => x + b[i]));
    return colSum
}
// function calculates diagonals
function calDiagonals(array) {
    let mainSum = 0,
        secondarySum = 0;
    for (let row = 0; row < array.length; row++) {
        mainSum += array[row][row];
        secondarySum += array[row][array.length - row - 1];
    }
    let diagonals = [mainSum, secondarySum]
    return diagonals
}

function checkGoal(array) {
    // get every row and column and diagonal sum
    let rows = calHor(array);
    let columns = calVer(array);
    let diagonals = calDiagonals(array);
    let rowsValue = 0,
        colValue = 0,
        diaValue = 0;
    // function checks if array values are equal
    const allEqual = array => array.every(v => v === array[0])
    // if all rows has the same value
    if (allEqual(rows)) {
        rowsValue = rows[0]
    }
    if (allEqual(columns)) {
        colValue = columns[0]
    }
    if (allEqual(diagonals)) {
        diaValue = diagonals[0]
    }
    // rows and columns and diagonals are equal return true else return false
    if (rowsValue = colValue = diaValue && rowsValue != 0) {
        return true;
    } else {
        return false;
    }
}

// defining initial state which is
// 2  0  6
// 0  5  1
// 4  3  0

// added 1 to square[1,2]
if (canPut(numbers, 1, 2, MagicNumber, MovesArray[MovesArray.length - 1])) {
    MovesArray.push(moveTo(numbers,MovesArray[MovesArray.length - 1], 1, 2))
    numbers.shift()
};
// added 2 to square[0,0]
if (canPut(numbers, 0, 0, MagicNumber, MovesArray[MovesArray.length - 1])) {
    MovesArray.push(moveTo(numbers,MovesArray[MovesArray.length - 1], 0, 0))
    numbers.shift()
};
// added 3 to square[2,1]
if (canPut(numbers, 2, 1, MagicNumber, MovesArray[MovesArray.length - 1])) {
    MovesArray.push(moveTo(numbers,MovesArray[MovesArray.length - 1], 2, 1))
    numbers.shift()
};
// added 4 to square[2,1]
if (canPut(numbers, 2, 1, MagicNumber, MovesArray[MovesArray.length - 1])) {
    MovesArray.push(moveTo(numbers,MovesArray[MovesArray.length - 1], 2, 0))
    numbers.shift()
};
// added 5 to square[1,1]
if (canPut(numbers, 1, 1, MagicNumber, MovesArray[MovesArray.length - 1])) {
    MovesArray.push(moveTo(numbers,MovesArray[MovesArray.length - 1], 1, 1))
    numbers.shift()
};
// added 6 to square[0,2]
if (canPut(numbers, 0, 2, MagicNumber, MovesArray[MovesArray.length - 1])) {
    MovesArray.push(moveTo(numbers,MovesArray[MovesArray.length - 1], 0, 2))
    numbers.shift()
};


// remove // to run using one of the two algorithms
// dfs(MovesArray, numbers)
// bfs(MovesArray, numbers)

// give it to matrices and check if they are the same
function compareMatrices(array, array2) {
    let bool = true
    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            if (array[i][j] != array2[i][j]) bool = false;
        }
    }
    return bool
}


function getAllNextSteps(square, numbers, open, close) {
    console.log('entered getAll');
    // move on every number remaining 
    numbers.map((val,numIndex)=>{
        //move on every cell in square
        square.map((row, i) => {
            row.map((el, j) => {
                // if cell is empty
                if (el == 0) {
                    //check if we can put number in this cell
                    if (canPut(numbers, i, j, MagicNumber, square)) {
                        let temp = []
                        // moved number to a copy of the given square
                        const newSquare = moveTo(numbers, square, i, j)
                        // removed the added number from the numbers array
                        numbers.splice(numIndex,1)
                        // push the new square to temp
                        temp.push(newSquare)
                        // pushed the remaining numbers to temp
                        temp.push(JSON.parse(JSON.stringify(numbers)))
                        // pushed temp to open array 
                        open.push(temp)
                    }
                    if(numbers[0]!=val){
                        // if the number were removed readd the number to remaining numbers again
                        numbers.splice(numIndex,0,val)
                    }
                }   
            })
        })
    })
}

function bfs(MovesArray, numbers) {
    
    console.log('entered bfs');

    let open = [], close = [],potato = [], success = false;
    // get initial square before algorithm
    let initialState = JSON.parse(JSON.stringify(MovesArray[MovesArray.length - 1]))
    updateArray(initialState, potato)
    updateArray(numbers,potato)
    updateArray(potato,open)
    
    while (open.length != 0) {
        let u = open.shift()
        if (checkGoal(u[0])) {
            success = true
            return console.log('You Won!');
        } 
        else {
            updateArray(u[0], close);
            getAllNextSteps(u[0], JSON.parse(JSON.stringify(u[1])), open, close)
        }
    }
    if (open.length == 0) {
        return console.log("didn't find Solution");
    }

}

function dfs(MovesArray, numbers) { 
    console.log('entered dfs');

    let open = [], close = [],potato = [], success = false;
    let initialState = JSON.parse(JSON.stringify(MovesArray[MovesArray.length - 1]))
    updateArray(initialState, potato)
    updateArray(numbers,potato)
    updateArray(potato,open)
    
    while (open.length != 0) {
        let u = open.pop()
        if (checkGoal(u[0])) {
            success = true
            return console.log('You Won!');
        } 
        else {
            updateArray(u[0], close);
            getAllNextSteps(u[0], JSON.parse(JSON.stringify(u[1])), open, close)
        }
    }
    if (open.length == 0) {
        return console.log("didn't find Solution");
    }

}